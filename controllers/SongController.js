var Song = require('./models/Song');

module.exports = {
	songs: function(req, res) {
    console.log(req.query.title);

    Song.findAll({
        where: {
            title: { like: '%' + req.query.title + '%' },
            playCount: { gt: 0 }
        },
        order: 'playCount DESC'
    }).then(function(results) {
        res.render('songs', {
            songs: results
        });
    });
	},

	create: function(req, res) {
		
	}
};